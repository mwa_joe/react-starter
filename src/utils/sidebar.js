import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from '@material-ui/core/ListSubheader';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { NavLink } from "react-router-dom";

import *  as icons from '@material-ui/icons';
import Icon from '@material-ui/core/Icon';


// Styling
const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    background: theme.palette.background.paper,
    paddingBottom: theme.spacing.unit * 4
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
  sizedBox: {
    height: 100,
    border: "1px solid black"
  }
});

class Sidebar extends React.Component {

  constructor(props) {
    super();
  }

  state = {};

  handleClick = (e) => {
    this.setState({ [e]: !this.state[e] });
  };

  renderMenu(list) {
    const classes = styles;

    /* For each list item */
    // items.list.map((list) => {
    return <List className={classes.root} key={list.id} subheader={<ListSubheader></ListSubheader>}>
      {/* For each item in each list item */}
      {
        list.items.map(
          (item) => {

            const icon = item.icon;

            return (
              <div key={item.id}>

                {/* If yes submenu items */}
                {item.subitems != null ?
                  (
                    <div key={item.id} className={classes.root}>

                      <ListItem
                        button key={item.id}
                        onClick={this.handleClick.bind(this, item.name)} >
                        <ListItemIcon>
                          <Icon>{icon}</Icon>
                        </ListItemIcon>
                        <ListItemText primary={item.name} />
                        {this.state[item.name] ? <ExpandLess /> : <ExpandMore />}
                      </ListItem>

                      <Collapse
                        key={list.items.id}
                        component="li"
                        in={this.state[item.name]}
                        timeout="auto"
                        unmountOnExit>

                        <List disablePadding>
                          {/* Submenu items */}
                          {item.subitems.map((sitem) => {
                            if (sitem.subitems) {
                              // add recursion here 
                            } else {
                              if (sitem.linkTo) {
                                return <NavLink to={sitem.linkTo ? "/" + sitem.linkTo + "/" : "#"}>
                                  <ListItem ListItem button key={sitem.id} className={classes.nested} >
                                    <ListItemIcon>
                                      {
                                        (sitem.name).split(" ")[1] != null ? (sitem.name).split(" ")[0].charAt(0) + (sitem.name).split(" ")[1].charAt(0) : (sitem.name).split(" ")[0].charAt(0)
                                      }
                                    </ListItemIcon>
                                    <ListItemText key={sitem.id} primary={sitem.name} />
                                  </ListItem>
                                </NavLink>
                              } else {
                                return <ListItem ListItem button key={sitem.id} className={classes.nested} >
                                  <ListItemIcon>
                                    {
                                      (sitem.name).split(" ")[1] != null ? (sitem.name).split(" ")[0].charAt(0) + (sitem.name).split(" ")[1].charAt(0) : (sitem.name).split(" ")[0].charAt(0)
                                    }
                                  </ListItemIcon>
                                  <ListItemText key={sitem.id} primary={sitem.name} />
                                </ListItem>
                              }
                            }
                          })}
                        </List>
                        <span className={classes.sizedBox}></span>
                      </Collapse>
                    </div>
                  )
                  :
                  (
                    <NavLink to={item.linkTo ? "/" + item.linkTo + "/" : "#"}>
                      <ListItem button onClick={this.handleClick.bind(this, item.name)} key={item.id}>
                        <ListItemIcon>
                          <Icon>{icon}</Icon>
                        </ListItemIcon>
                        <ListItemText primary={item.name} />
                      </ListItem>
                    </NavLink>
                  )
                }
              </div>
            )
          }
        )
      }
      {/* <Divider key={list.id} absolute /> */}
    </List >;

  };

  render() {
    const classes = styles;
    const items = this.props.menuItems;

    return (
      <div className={classes.root}>
        {items.list.map((item) => this.renderMenu(item))}
      </div >
    );
  }
}
Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Sidebar);