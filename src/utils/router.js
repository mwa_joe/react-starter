import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "../ui/components/home/home";
import Admin from "../ui/components/admin/admin";
import TimelinePage from '../ui/components/pages/timeline';
import Dashboard from '../ui/components/admin/dashboard';

class MainRouter extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/admin" component={Admin} />
        </Switch>
      </Router>
    );
  }
}

class DashboardContentRouter extends Component {
  render() {
    return (
      <Switch>
        <Route path="/admin/timeline" component={TimelinePage} />
        <Route path="/admin/dashboard" component={Dashboard} />
      </Switch>
    );
  }
}
export {
  MainRouter,
  DashboardContentRouter
}