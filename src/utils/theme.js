import { createMuiTheme } from '@material-ui/core';

const myTheme = createMuiTheme({
    palette: {
        primary: {
            main: "#023C40",
        },
        secondary: {
            main: "#F39237"
        }
    },
});

// See what you can do and what you have done
// console.log(myTheme);

const theme = function () {
    return myTheme;
}

export default theme;