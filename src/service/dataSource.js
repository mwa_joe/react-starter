// Main menu item
function getFirstItem() {
  var json = {
    "list": [
      {
        "id": "1",
        "title": "Google",
        "items": [
          {
            "id": "1",
            "icon": "account_circle_two_tone_icon",
            "name": "Joe Mwa",
            "subitems": [
              {
                "id": "1",
                "icon": "mail_icon",
                "name": "My Profile",
                "linkTo": "admin/timeline",
              },
              {
                "id": "2",
                "icon": "mail_icon",
                "name": "Edit Settings"
              },
              {
                "id": "3",
                "icon": "mail_icon",
                "name": "Settings"
              }
            ]
          },
        ]
      },
    ]
  };
  return json;
}

// Menu items
function getItems() {
  var json = {
    "list": [
      {
        "id": 1,
        "title": "dashboard",
        "items": [
          {
            "id": 1,
            "icon": "dashboard",
            "name": "Dashboard",
            "linkTo": "admin/dashboard"
          }
        ]
      },
      {
        "id": 2,
        "title": "under_dashboard",
        "items": [
          {
            "id": 1,
            "icon": "image_icon",
            "name": "Pages",
            "subitems": [
              {
                "id": 1,
                "icon": "mail_icon",
                "name": "Pricing Page Page",
                "linkTo": "admin/timeline",
                "subitems": [
                  {
                    "id": 1,
                    "icon": "mail_icon",
                    "name": "Pricing Page Sub",
                    "linkTo": "admin/timeline"
                  },
                ]
              },
              {
                "id": 2,
                "icon": "mail_icon",
                "name": "RTL Support",
                "linkTo": "admin/timeline"
              },
              {
                "id": 3,
                "icon": "mail_icon",
                "name": "Timeline Page",
                "linkTo": "admin/timeline"
              },
              {
                "id": 4,
                "icon": "mail_icon",
                "name": "Login Page"
              },
              {
                "id": 5,
                "icon": "mail_icon",
                "name": "Register Page"
              },
              {
                "id": 6,
                "icon": "mail_icon",
                "name": "Lock Screen Support"
              },
              {
                "id": 7,
                "icon": "mail_icon",
                "name": "User Profile"
              },
              {
                "id": 8,
                "icon": "mail_icon",
                "name": "Error Page"
              },

            ]
          },
          {
            "id": 2,
            "icon": "apps_icon",
            "name": "Components",
            "subitems": [
              {
                "id": 1,
                "icon": "mail_icon",
                "name": "Buttons"
              },
              {
                "id": 2,
                "icon": "mail_icon",
                "name": "Grid System"
              },
              {
                "id": 3,
                "icon": "mail_icon",
                "name": "Panels"
              },
              {
                "id": 4,
                "icon": "mail_icon",
                "name": "Sweet Alert"
              },
              {
                "id": 5,
                "icon": "mail_icon",
                "name": "Notifications"
              },
              {
                "id": 6,
                "icon": "mail_icon",
                "name": "Icons"
              },
              {
                "id": 7,
                "icon": "mail_icon",
                "name": "Typography"
              }
            ]
          },
          {
            "id": 3,
            "icon": "assignmentIcon",
            "name": "Forms",
            "subitems": [
              {
                "id": 1,
                "icon": "mail_icon",
                "name": "Regular Forms"
              },
              {
                "id": 2,
                "icon": "mail_icon",
                "name": "Extended Forms"
              },
              {
                "id": 3,
                "icon": "mail_icon",
                "name": "Validation Forms"
              },
              {
                "id": 4,
                "icon": "mail_icon",
                "name": "Wizard"
              }
            ]
          },
          {
            "id": 4,
            "icon": "grid_on_icon",
            "name": "Tables",
            "subitems": [
              {
                "id": 1,
                "icon": "mail_icon",
                "name": "Regular Tables"
              },
              {
                "id": 2,
                "icon": "mail_icon",
                "name": "Extended Tables"
              },
              {
                "id": 3,
                "icon": "mail_icon",
                "name": "React Tables"
              },
              {
                "id": 4,
                "icon": "mail_icon",
                "name": "Wizard"
              }
            ]
          },
          {
            "id": 5,
            "icon": "location_on_icon",
            "name": "Maps",
            "subitems": [
              {
                "id": 1,
                "icon": "mail_icon",
                "name": "Google Maps"
              },
              {
                "id": 2,
                "icon": "mail_icon",
                "name": "FullScreen Maps"
              },
              {
                "id": 3,
                "icon": "mail_icon",
                "name": "Vector Maps"
              },
            ]
          }
        ]
      },
      {
        "id": 4,
        "icon": "mail_icon",
        "title": "others",
        "items": [
          {
            "id": 1,
            "icon": "widgets_icon",
            "name": "Widgets"
          },
          {
            "id": 2,
            "icon": "timeline_icon",
            "name": "Charts"
          },
          {
            "id": 3,
            "icon": "date_range_icon",
            "name": "Calendar"
          }
        ]
      }
    ]
  };
  return json;
}

export { getFirstItem, getItems };