import { createDrawerNavigator } from 'react-navigation';
import HomePage from '../pages/pages/home';
import TimelinePage from '../pages/pages/timeline';

const DrawerNavigator = createDrawerNavigator(
    {
        Home: HomePage,
        Settings: TimelinePage,
    },
    {
        hideStatusBar: true,
        drawerBackgroundColor: 'rgba(255,255,255,.9)',
        overlayColor: '#6b52ae',
        contentOptions: {
            activeTintColor: '#fff',
            activeBackgroundColor: '#6b52ae',
        },
    }
);

export default createAppContainer(DrawerNavigator);