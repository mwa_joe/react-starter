import React, { Component } from "react";

class HomePage extends Component {

  constructor(props) {
    super();
  }

  render() {

    function Welcome(props) {
      return <span> {props.name} </span>
    }

    return (
      <div>
        <div> Welcome <Welcome name={this.props.name} /></div>
      </div>
    )
  }
}

export default HomePage;