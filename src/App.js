import React from 'react';
import './App.css';

import Home from "./ui/components/home/home";

import { MainRouter } from "./utils/router";
import theme from "./utils/theme";
import { MuiThemeProvider } from '@material-ui/core';

function App() {
  return (
    <div className="App">
      <MuiThemeProvider theme={theme()}>
        <MainRouter>
          <Home name="Joe" />
        </MainRouter>
      </MuiThemeProvider>
    </div>
  );
}
export default App;